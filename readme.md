## Ark's Proxy Scraper
Pure java HTTP/HTTPS proxy scraping utility
## Use
Run with the `-h` flag for full list of features.
## Build
Run `mvn clean package` or the package process in your IDE.
