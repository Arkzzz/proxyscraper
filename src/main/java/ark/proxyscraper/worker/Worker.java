package ark.proxyscraper.worker;

import ark.proxyscraper.util.Http;

import java.util.ArrayList;

public class Worker extends Thread {
    private String[] proxies;
    public Worker(String[] proxies) {
        this.proxies = proxies;
    }

    public void run() {
        super.run();
        for(String proxy : this.proxies) {
            try {
                String res = Http.proxiedGet("http://httpbin.org/ip", 3000, proxy);
                if (!res.equals("")) {
                    this.submit(proxy);
                }
            } catch (Exception e) { }
        }
    }

    protected void submit(String proxy) { }
}
