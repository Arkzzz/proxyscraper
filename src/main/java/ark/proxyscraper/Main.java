package ark.proxyscraper;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] rawArgs) {
        System.out.println("--- Ark's Proxy Scraper ---");
        ArrayList<String> args = new ArrayList<String>(Arrays.asList(rawArgs));

        if (args.contains("-h") || args.contains("-help")) {
            System.out.println(
                    "Help:"
                    + "Usage - arguments: \n"
                    + "h / help - Prints help message."
                    + "p / providers - The list of sites that you plan on scraping."
                    + "to - The output file."
                    + "threads - The number of threads to use while scraping."
                    + "nocheck - Disable proxy checking."
            );
            System.exit(0);
        }

        String providers = "urls.txt";
        String output = "proxies.txt";
        int threads = 25;
        boolean check = true;

        for (int i = 0; i < args.size(); i++) {
            switch ("-" + args.get(i)) {
                case "p": providers = args.get(i + 1);
                case "providers": providers = args.get(i + 1);
                case "to": output = args.get(i + 1);
                case "threads": threads = Integer.parseInt(args.get(i + 1));
                case "nocheck": check = false;
            }
        }

        new Scraper(providers, output, threads, check).scrape();
    }
}
