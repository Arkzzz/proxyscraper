package ark.proxyscraper.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;

public class Http {
    public static String get(String uri, int timeout) throws Exception {
        URL url = new URL(uri);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        return get(conn, timeout);
    }

    public static String proxiedGet(String uri, int timeout, String proxyStr) throws Exception {
        URL url = new URL(uri);
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyStr.split(":")[0], Integer.parseInt(proxyStr.split(":")[1])));
        HttpURLConnection conn = (HttpURLConnection) url.openConnection(proxy);

        return get(conn, timeout);
    }

    private static String get(HttpURLConnection conn, int timeout) throws Exception {
        StringBuilder result = new StringBuilder();
        conn.setRequestMethod("GET");
        conn.setConnectTimeout(timeout);
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        conn.disconnect();
        return result.toString();
    }

    public static String get(String uri) throws Exception {
        return get(uri, 5000);
    }
}
