package ark.proxyscraper.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class FS {
    public static String read(String path) throws Exception {
        StringBuilder result = new StringBuilder();
        File file = new File(path);
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);

        String str;
        while ((str = br.readLine()) != null) {
            result.append(str + "\n");
        }
        fr.close();
        br.close();
        return result.toString();
    }
}
