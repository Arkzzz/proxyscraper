package ark.proxyscraper;

import ark.proxyscraper.util.FS;
import ark.proxyscraper.util.Http;
import ark.proxyscraper.worker.Worker;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Scraper {
    private ArrayList<String> proxies = new ArrayList<>();
    private ArrayList<String> providers;
    private BufferedWriter output;
    private int MAX_THREADS;
    private boolean check;
    private int checkedCounter = 0;

    Scraper(String providersPath, String outputPath, int threads, boolean check) {
        this.MAX_THREADS = threads;
        this.check = check;
        try {
            String raw = FS.read(providersPath);
            this.providers = new ArrayList<>(Arrays.asList(raw.split("\n")));
        } catch (Exception e) {
            System.err.println("Error while reading providers!");
            e.printStackTrace();
        }

        try {
            this.output = new BufferedWriter(new FileWriter(outputPath));
            this.output.write("--- Ark's Proxy Scraper ---" + "\n");
        } catch (Exception e) {
            System.err.println("Error while getting file output stream!");
        }
    }

    void scrape() {
        Pattern proxyRegex = Pattern.compile("(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|25[0-5]|2[0-4]\\d):0*(?:6553[0-5]|655[0-2][0-9]|65[0-4][0-9]{2}|6[0-4][0-9]{3}|[1-5][0-9]{4}|[1-9][0-9]{1,3}|[0-9])");
        for (String provider : providers) {
            try {
                System.out.println("Querying provider: " + provider);
                String res = Http.get(provider);
                Matcher m = proxyRegex.matcher(res);
                while(m.find()) this.proxies.add(m.group());
            } catch (Exception e) {
                System.err.println("Error couldn't contact provider @ " + provider);
            }
        }

        if (this.check) checkProxies();
        else {
            try {
                for(String proxy : this.proxies) this.output.write(proxy + "\n");
                this.output.close();
            } catch (Exception e) {
                System.err.println("Error while writing proxies to file!");
                e.printStackTrace();
            }
        }
    }

    private void checkProxies() {
        String[] genericArray = new String[this.proxies.size()];
        genericArray = this.proxies.toArray(genericArray);
        final int totalProxies = genericArray.length;
        final int proxiesPerWorker = (int) Math.floor(totalProxies / MAX_THREADS);
        for (int i =0; i < MAX_THREADS; i++) {
            String[] proxies = new String[proxiesPerWorker];
            System.arraycopy(genericArray, proxiesPerWorker * i, proxies, 0, proxiesPerWorker);
            new Worker(proxies) {
                @Override
                protected void submit(String proxy) {
                    append(proxy);
                    checkedCounter++;
                }
            }.start();
        }

        new Thread(() -> {
             while (true) {
                 System.out.println("Good: " + checkedCounter + "/" + totalProxies);
                 try {
                     this.output.flush();
                     Thread.sleep(5000);
                 } catch (Exception e) {
                     e.printStackTrace();
                 }
             }
        }).start();
    }

    private void append(String str) {
        try {
            this.output.write(str + "\n");
        } catch (Exception e) {
            System.err.println("Error while writing proxies to file!");
        }
    }


}
